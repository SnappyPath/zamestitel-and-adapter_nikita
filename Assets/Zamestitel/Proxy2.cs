﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proxy2 : ISubject
{
    private GameInfo _realSubject;
    public Proxy2(GameInfo realSubject)
    {
        this._realSubject = realSubject;
    }

    public void CompleteLvl()
    {
        if (this.CheckAccess())
        {
            this._realSubject.CompleteLvl();
        }
    }

    public bool CheckAccess()
    {
        _realSubject.Score = _realSubject.Score + _realSubject.CurrentLvl * 100;

        return true;
    }
}
