﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfo : ISubject
{
    public  int CurrentLvl;
    public int Score;


    public void CompleteLvl()
    {
        CurrentLvl += 1;
    }

    public void ChangeScore(int value)
    {
        Score = Score + value; 
    }
}
