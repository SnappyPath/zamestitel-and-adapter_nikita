﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proxy : ISubject
{
    private GameInfo _realSubject;

    public Proxy(GameInfo realSubject)
    {
        this._realSubject = realSubject;
    }

    public void CompleteLvl()
    {
        if (this.CheckAccess())
        {
            this._realSubject.CompleteLvl();
        }
    }

    public bool CheckAccess()
    {
        Debug.Log("Current level: " + _realSubject.CurrentLvl);

        return true;
    }

}
