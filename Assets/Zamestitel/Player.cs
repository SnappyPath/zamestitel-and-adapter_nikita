﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private GameInfo Session;
    private Proxy proxy;
    private Proxy2 proxy2;

    private void Start()
    {
        Session = new GameInfo();
        proxy = new Proxy(Session);
        proxy2 = new Proxy2(Session);


        proxy.CompleteLvl();
        proxy2.CompleteLvl();

        Debug.Log(Session.Score);
        Debug.Log(Session.CurrentLvl);

    }
    public void CompleteLvl()
    {        
      this.Session.CompleteLvl();    
    }

}
