﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdapterBuilder : IWorker
{
    // Start is called before the first frame update
    private readonly Builder _builder;

    public AdapterBuilder(Builder builder)
    {
        this._builder = builder;
    }

    public void PayMoney(int v)
    {
        _builder.GetSalary(new Salary(v));
    }

    public void Work()
    {
        this._builder.Build();
    }

}
