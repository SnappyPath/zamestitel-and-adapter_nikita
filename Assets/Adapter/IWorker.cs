﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWorker
{
    void Work();

    public void PayMoney(int v);

}
