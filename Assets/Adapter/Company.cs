﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Company : MonoBehaviour
{
    public IWorker worker;
    void Start()
    {
        Builder b = new Builder();
        worker = new AdapterBuilder(b);
        worker.Work();
        worker.PayMoney(1000);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
